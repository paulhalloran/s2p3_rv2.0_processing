##################################################
# Specify the data you want to read in           #
##################################################

# input_file_directory_containing = '/disk2/ph290/s2p3/'
input_file_directory_containing = '/disk2/ph290/s2p3/computeserver/'

# output_file_directory = '/disk2/ph290/s2p3/processed/'
output_file_directory = '/disk2/ph290/s2p3/computeserver/processed/'

domain_file_for_run = '/home/ph290/s2p3/s2p3_rv2.0_forcing/s12_m2_s2_n2_h_map_1801803030_0.1.dat'
# input_file_base = 'HadGEM2-ES' # start of file name, to be followed by an underscore then the year number
# output_identifier = 'HadGEM2-ES'
input_file_base = 'global_tropics_era5' # start of file name, to be followed by an underscore then the year number
output_identifier = 'global_tropics_era5'
column_names = ["day","longitude", "latitude", "depth", "surface temperature","bottom temperature","surface chlorophyll","par surface","par bottom"]
#Note, the first three columns must always be "day","longitude", "latitude"
#Note, if the variable is a netcdf compliant variable (see http://cfconventions.org/Data/cf-standard-names/50/build/cf-standard-name-table.html, specify the name etc here, and set specifying_names to True:
columns_to_output=[4] # e.g. [1,3] would be longitude and depth
lon_values_0_to_360=True # you will priobably have to set this to true for global runs, or runs which go over the dateline, false for those which don't cross the dateline
specifying_names = False

## If specifying_names above is set to True, specify the below. If not, ignore ##
standard_name=['sea_surface_temperature','sea_surface_temperature','sea_surface_temperature','sea_surface_temperature','sea_surface_temperature']
long_name=['Sea Surface Temperature','Sea Surface Temperature','Sea Surface Temperature','Sea Surface Temperature','Sea Surface Temperature']
var_name=['tos','tos','tos','tos','tos']
units=['K','K','K','K','K']



##################################################
# Main program                                   #
##################################################

if input_file_base[-4::].isdigit():
    print 'Are you sure your input_file_base name does not contain the specific run year? If it does remove.'


import numpy as np
import iris
import matplotlib.pyplot as plt
import iris.quickplot as qplt
import glob
import pandas as pd
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
from cf_units import Unit
import sys
import multiprocessing as mp
from functools import partial

def put_data_into_cube(df,df_domain,variable,specifying_names,standard_name,long_name,var_name,units):
    latitudes = np.unique(df_domain['lat'].values)
    longitudes = np.unique(df_domain['lon'].values)
    latitudes_run = np.unique(df['latitude'].values)
    longitudes_run = np.unique(df['longitude'].values)
    times = np.unique(df['day'].values)
    latitude = iris.coords.DimCoord(latitudes, standard_name='latitude', units='degrees')
    longitude = iris.coords.DimCoord(longitudes, standard_name='longitude', units='degrees')
    time = iris.coords.DimCoord(times, standard_name='time', units='days')
    time = iris.coords.DimCoord(times, standard_name='time', units=Unit('days since '+run_start_date+' 00:00:0.0', calendar='gregorian'))
    if specifying_names:
        cube = iris.cube.Cube(np.zeros((times.size,latitudes.size, longitudes.size), np.float32),standard_name=standard_name, long_name=long_name, var_name=var_name, units=units,dim_coords_and_dims=[(time,0), (latitude, 1), (longitude, 2)])
    else:
        cube = iris.cube.Cube(np.zeros((times.size,latitudes.size, longitudes.size), np.float32),standard_name=None, long_name=None, var_name=None, units=None,dim_coords_and_dims=[(time,0), (latitude, 1), (longitude, 2)])
    Z,X,Y = np.meshgrid(cube.coord('time').points,cube.coord('longitude').points,cube.coord('latitude').points)
    data = cube.data.copy()
    data[:] = -999.99
    days = np.unique(df['day'].values)
    shape = [X.shape[0],X.shape[2]]
    for i,day in enumerate(days):
        # print 'processing day: ',i
        df_tmp = df.loc[df['day'] == day]
        # if len(df_tmp) < shape[0]*shape[1]:
        #     #sometimes depending on the grid, the dataset is one value too small. This just adds missing data to an additional last line
        #     missing_df=df_tmp.iloc[0:1]
        #     missing_df=missing_df.replace(missing_df,-999.99)
        #     df_tmp = pd.concat([df_tmp, missing_df])
        for j,lat in enumerate(df_tmp['latitude'].values):
            # print j
            lon = df_tmp['longitude'].values[j]
            lat_loc = np.where(np.around(cube.coord('latitude').points,decimals=6) == np.around(lat,decimals=6))[0][0]
            # lon_loc = np.where(np.around(cube.coord('longitude').points,decimals=6) == np.around(lon-360.0,decimals=6))[0][0]
            lon_loc = np.where(np.around(cube.coord('longitude').points,decimals=6) == np.around(lon,decimals=6))[0][0]
            data[i,lat_loc,lon_loc] = df_tmp[variable].values[j]
        # data[i,:,:] = np.fliplr(np.rot90(df_tmp[variable].values.reshape(shape),3))
    data = np.ma.masked_where((data.data < -999.9) & (data.data > -1000.0),data)
    # data = np.ma.masked_where(data.data == 0.0,data)
    cube.data = data
    cube.data.fill_value = -999.99
    cube.data.data[~(np.isfinite(cube.data.data))]=-999.99
    cube.data = np.ma.masked_where(cube.data == -999.99,cube.data)
    return cube


if not(specifying_names):
    standard_name=np.tile('sea_surface_temperature',len(column_names))
    long_name=np.tile('Sea Surface Temperature',len(column_names))
    var_name=np.tile('tos',len(column_names))
    units=np.tile('K',len(column_names))


fwidths=[8,8,6,6,6,6,6,6,6,6,6,6,8]
print 'reading in lats and lons from domain file'
df_domain = pd.read_fwf(domain_file_for_run,names=['lon','lat','t1','t2','t3','t4','t5','t6','t7','t8','t9','t10','depth'],widths = fwidths,
                 skiprows=[0],dtype={'lon':float,'lat':float,'t1':float,'t2':float,'t3':float,'t4':float,'t5':float,'t6':float,'t7':float,'t8':float,'t9':float,'t10':float,'depth':float},usecols=['lon','lat','depth'])

files = sorted(glob.glob(input_file_directory_containing + input_file_base+'_????'))

df = pd.read_csv(files[0],header=None, sep=r'\s{1,}')
if len(list(df)) != len(column_names):
    print 'number of column names specified here does not match number of columns in input file. Exiting.'
    sys.exit()


def output_the_data(df,df_domain,column_names,specifying_names,standard_name,long_name,var_name,units,output_identifier,year,i):
    print 'processing: '+column_names[i]
    output_cube = put_data_into_cube(df,df_domain,column_names[i],specifying_names,standard_name[i],long_name[i],var_name[i],units[i])
    output_filename = '_'.join(column_names[i].split(' '))+'_'+output_identifier+'_'+year+'.nc'
    # write out the data
    iris.fileformats.netcdf.save(output_cube, output_file_directory+output_filename, zlib=True, complevel=4)


for file in files:
    year = file.split('_')[-1]
    print 'processing year: '+year

    run_start_date = year+'-01-01'

    #########
    # Read data in
    #########
    df = pd.read_csv(file,header=None, sep=r'\s{1,}',names = column_names)
    if lon_values_0_to_360:
      df['longitude'] -= 180.0
    #########
    # Put data into an iris cube and save it
    #########

    #for i,column_name in enumerate(column_names[3::]):
    #    print 'processing: '+column_name
    #    output_cube = put_data_into_cube(df,df_domain,column_name,specifying_names,standard_name[i],long_name[i],var_name[i],units[i])
    #    output_filename = '_'.join(column_name.split(' '))+'_'+output_identifier+'_'+year+'.nc'
    #    # write out the data
    #    iris.fileformats.netcdf.save(output_cube, output_file_directory+output_filename, zlib=True, complevel=4)

    num_procs = mp.cpu_count()
    pool = mp.Pool(processes = np.min([14,num_procs]))
    func = partial(output_the_data, df,df_domain,column_names,specifying_names,standard_name,long_name,var_name,units,output_identifier,year)
    # results = pool.map(func, range(len(column_names[3::])))
    results = pool.map(func, columns_to_output)

# import iris.quickplot as qplt
# qplt.pcolormesh(output_cube[0])
# plt.gca().coastlines()
# plt.show()
