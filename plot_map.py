######################################################
# edit lines here to specify file name and plot type #
######################################################

file_to_plot = 'surface_temperature.nc'
#two options are provided as examples for analysis here (plotting the time meaned map or plotting a specific day of the run)
#but see http://scitools.org.uk/iris/docs/v1.9.0/html/examples/index.html and http://scitools.org.uk/iris/docs/v1.9.0/html/gallery.html for more examples
plot_name = 'my_plot'
#to plot the time-averaged map (set to False if you want to plot a specific day)
time_average = True
#to plot a specific day:
day_to_plot = 150

######################################################
# main code                                          #
######################################################

import iris
import iris.quickplot as qplt
import matplotlib.pyplot as plt
from matplotlib.colors import BoundaryNorm
from matplotlib.ticker import MaxNLocator
import numpy as np

#read in file
cube =iris.load_cube(file_to_plot)
if time_average:
    plot_cube = cube.collapsed('time',iris.analysis.MEAN)
else:
    plot_cube = cube[day_to_plot]

#define colour palette and colour levels
levels = MaxNLocator(nbins=50).tick_values(np.nanmin(plot_cube.data), np.nanmax(plot_cube.data))
cmap = plt.get_cmap('Reds')
norm = BoundaryNorm(levels, ncolors=cmap.N, clip=True)

#produce quick plotting
qplt.pcolormesh(plot_cube,cmap=cmap, norm=norm)
plt.gca().coastlines('50m')
plt.savefig('./'+plot_name+'.png')
plt.show()
