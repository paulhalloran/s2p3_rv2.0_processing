# README for processing and plotting output from s2p3_rv2.0

Note, if you have run the model with the option 'generate_netcdf_files' set to True, this step will not be required

The instructions here assume you are working on a linux machine

############################################################
# Requirements                                             #
############################################################


- git (https://gist.github.com/derhuerst/1b15ff4652a867391f03)
  - install with:

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install git

- Python2.7 with additional libraries (see below)

   - installing conda will make this easier https://conda.io/docs/user-guide/install/index.html

      - additional libraries can then be installed with (note some of these may already be installed or be installed by others):

conda install pandas

conda install numpy

conda install -c conda-forge iris

conda install -c conda-forge cartopy

conda install matplotlib

- CDO (https://code.mpimet.mpg.de/projects/cdo/)

   - see instructions in https://bitbucket.org/paulhalloran/s2p3_rv2.0_forcing


############################################################
# Installation                                             #
############################################################

git clone https://paulhalloran@bitbucket.org/paulhalloran/s2p3_rv2.0_processing.git

cd s2p3_rv2.0_processing

############################################################
# Copy model output data to your directory                 #
############################################################

if s2p3_rv2.0 is in example_location_1 (e.g.) username@remote_cluster:~/ and s2p3_rv2.0_processing is in example_location_2 (e.g.) ~/Documents

and assuming your have stuck with the standard model output file name of output_map

scp example_location_1/s2p3_rv2.0/output/output_map example_location_2/s2p3_rv2.0_processing

s2p3_rv2.0_processing will now contain the model output file output_map

############################################################
# Generating netcdf files                                  #
############################################################

edit the first few lines in generate_netcdf_files.py to specify the names of the input and output files etc.

run the script with

python2.7 generate_netcdf_files.py

############################################################
# plotting  data                                           #
############################################################

A very simple plotting script is included as an example

edit the first few lines of plot_map.py to specify the file you want to plot, the type of plot_map etc.

then run with

python2.7 plot_map.py
